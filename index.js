
// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Створення одного об'єкта на підставі іншого трохи змінивши його властивості

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// У конструкторі ключове слово super() використовується як функція, що викликає батьківський конструктор або функцію батьківського об'єкта.


class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name[0].toUpperCase() + this._name.slice(1).toLowerCase();
  }
  set name(nemName) {
    this._name = nemName;
  }
  get age() {
    return this._age + ' years';
  }
  set age(nemAge) {
    this._age = nemAge;
  }

  get salary() {
    return this._salary;
  }
  set salary(nemSalary) {
    this._salary = nemSalary;
  }
}
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return super.salary * 3;
  }
  info = () =>
    console.log(`name: ${this.name}, age: ${this.age}, salary: ${this.salary}, lang: ${this.lang}`);
}
const bob = new Programmer("bob", 25, 10000, "C++");
bob.info();

const sam = new Programmer("sam", 40, 40000, "JS");
sam.info();

const pam = new Programmer("pamella", 35, 30000, "JS");
pam.info();
